.. _tutorial:

###################
The FEniCS Tutorial
###################

A new version of Hans Petter Langtangen's FEniCS Tutorial, the opening
chapter of the FEniCS book, is currently being prepared. The book is
expected to be completed in the fall of 2016.

You can `access the preliminary version of the tutorial <https://fenicsproject.org/pub/tutorial/fenics-tutorial-vol1-2016-11-03.pdf>`__.

Comments and corrections can be reported as issues for the `Git repository of the book <https://github.com/hplgit/fenics-tutorial>`__ or via email to logg@chalmers.se.
